﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasketGuessGame.Services.Players
{
    class MemoryPlayer : Player
    {
        List<int> guessedNumbers = new List<int>();
        public MemoryPlayer(GameManagerService bascet, string name)
        {
            managerService = bascet;
            this.name = name;
        }
        public override void Play()
        {

            if (skipCount > 0)
            {
                skipCount--;
                return;
            }

            var random = new Random();
            var randomNumber = random.Next(40, 140);
            while (guessedNumbers.Contains(randomNumber))
            {
                randomNumber = random.Next(40, 140);
            }
            guessedNumbers.Add(randomNumber);
            skipCount = managerService.TryPlay(randomNumber, name);
        }
    }
}
