﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasketGuessGame.Services.Players
{
    class RandomPlayer: Player
    {
        public RandomPlayer(GameManagerService bascet, string name)
        {
            managerService = bascet;
            this.name = name;
        }
        public override void Play()
        {
            if (skipCount > 0)
            {
                skipCount--;
                return;
            }
            var randomNumber = new Random().Next(40, 140);
            skipCount = managerService.TryPlay(randomNumber, name);
        }
        
    }
}
