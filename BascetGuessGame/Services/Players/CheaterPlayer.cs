﻿using BasketGuessGame.Services;
using BasketGuessGame.Services.Players;
using System;
using System.Collections.Generic;
using System.Text;

namespace BascetGuessGame.Services.Players
{
    class CheaterPlayer : Player
    {
        public CheaterPlayer(GameManagerService bascet, string name)
        {
            managerService = bascet;
            this.name = name;
        }
        public override void Play()
        {

            if (skipCount > 0)
            {
                skipCount--;
                return;
            }
            var random = new Random();
            var randomNumber = random.Next(40, 140);
            while (managerService.propossedNumbers.Contains(randomNumber))
            {
                randomNumber = random.Next(40, 140);
            }
            skipCount = managerService.TryPlay(randomNumber, name);
        }
    }
}
