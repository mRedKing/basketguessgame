﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasketGuessGame.Services.Players
{
    abstract class Player
    {
        public abstract void Play();
        protected int skipCount = 0;
        protected string name = "";
        protected GameManagerService managerService;
    }
}
