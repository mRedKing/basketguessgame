﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasketGuessGame.Services.Players
{
    class ThoroughPlayer: Player
    {
        List<int> options = new List<int>();
        int currentOptionIndex = 0;
        public ThoroughPlayer(GameManagerService bascet, string name)
        {
            managerService = bascet;
            this.name = name;
            for(int i = 40;i <=140; i++)
            {
                options.Add(i);
            }
        }
        public override void Play()
        {
            if (skipCount > 0)
            {
                skipCount--;
                return;
            }
            var guessNumber = options[currentOptionIndex];
            currentOptionIndex++;
            skipCount = managerService.TryPlay(guessNumber, name);
        }
    }
}
