﻿using BasketGuessGame.Services;
using BasketGuessGame.Services.Players;
using System;
using System.Collections.Generic;
using System.Text;

namespace BascetGuessGame.Services.Players
{
    class ThoroughCheaterPlayer : Player
    {
        public ThoroughCheaterPlayer(GameManagerService bascet, string name)
        {
            managerService = bascet;
            this.name = name;
        }
        public override void Play()
        {

            if (skipCount > 0)
            {
                skipCount--;
                return;
            }

            var randomNumber = 40;
            while (managerService.propossedNumbers.Contains(randomNumber))
            {
                randomNumber++;
            }
            skipCount = managerService.TryPlay(randomNumber, name);
        }
    }
}
