﻿using BascetGuessGame.Services.Players;
using BasketGuessGame.Services.Players;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasketGuessGame.Services
{
    class GameManagerService
    {
        private static GameManagerService instance;

        private int _weight;
        private int _attempt = 0;
        private string _winnerName;
        private int _closeGuess = 0;
        private string _almostWinnerName;
        private bool _isGameFnished;
        public List<int> propossedNumbers = new List<int>();

        public static GameManagerService getInstance()
        {
            if (instance == null)
                instance = new GameManagerService();
            return instance;
        }
        public void StarGameProcess()
        {
            List<Player> players = DisplayMenu();

            StartGame();
            Console.WriteLine($"Real weight - {_weight}");

            while (!_isGameFnished && _attempt < 100)
            {
                _attempt++;
                players.ForEach(p =>
                {
                    if (_isGameFnished) { return; }
                    p.Play();
                });
            }
            if(_winnerName != null)
            {
                Console.WriteLine($"Winner {_winnerName},attempt - {_attempt}");
            } 
            else
            {
                Console.WriteLine($"Almost winner {_almostWinnerName} guess - {_closeGuess},attempt - {_attempt}");
            }
            Console.WriteLine($"Fin");
        }
        
        public int TryPlay(int guess, string playerName)
        {
            propossedNumbers.Add(guess);
            if (IsWeightCorrect(guess, playerName)) 
            {
                _winnerName = playerName;
            }
            return CountSkipRounds(guess);
        }
        private bool IsWeightCorrect(int guessweight, string playerName)
        {
            if (_isGameFnished)
            {
                return false;
            }
            if (Math.Abs(guessweight - _weight) < Math.Abs(_closeGuess - _weight))
            {
                _closeGuess = guessweight;
                _almostWinnerName = playerName;
            }
            _isGameFnished = (guessweight == _weight);
            return _isGameFnished;
        }
        private int CountSkipRounds(int guess)
        {
            return ((Math.Abs(_weight - guess) / 10) - 1);
        }
        private void SetWeight(int weight)
        {
            if (weight >= 40 && weight <= 140)
            {
                this._weight = weight;
            }
        }

        private void StartGame()
        {
            Random random = new Random();
            int startWeight = random.Next(40, 140);
            SetWeight(startWeight);
        }
        private List<Player> DisplayMenu()
        {

            var isContinue = true;
            int menu = 0;
            int subMenu = 0;
            List<Player> players = new List<Player>();
            while (isContinue && players.Count < 2)
            {
                Console.WriteLine("1. Add player");
                Console.WriteLine("2. Play");
                try {
                    menu = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Please enter number (1,2)");
                }

                switch (menu)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter Player Name");
                            string name = Console.ReadLine();
                            Console.WriteLine("Select Player type\n1.Random \n2.Memory \n3.Thorough \n4.Cheater \n5.Thorough Cheater");
                            try
                            {
                                subMenu = Convert.ToInt32(Console.ReadLine());
                            }
                            catch
                            {
                                Console.WriteLine("Please enter number (1-5)");
                            }
                            Player player;
                            switch (subMenu)
                            {
                                case 1:
                                default:
                                    player = new RandomPlayer(getInstance(), name);
                                    break;
                                case 2:
                                    player = new MemoryPlayer(getInstance(), name);
                                    break;
                                case 3:
                                    player = new ThoroughPlayer(getInstance(), name);
                                    break;
                                case 4:
                                    player = new CheaterPlayer(getInstance(), name);
                                    break;
                                case 5:
                                    player = new ThoroughCheaterPlayer(getInstance(), name);
                                    break;
                            }
                            players.Add(player);
                            if (players.Count >= 8)
                            {

                                Console.WriteLine("8 players is maximum");
                                isContinue = false;
                                break;
                            }
                        }
                        break;

                    case 2:
                    default:
                        if (players.Count < 2)
                        {
                            Console.WriteLine("Please add at least 2 players");
                            break;
                        }
                        isContinue = false;
                        break;
                }
            }
            return players;
        }

    }
}
