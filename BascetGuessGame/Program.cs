﻿using BasketGuessGame.Services;
using System;

namespace BasketGuessGame
{
    class Program
    {
        static void Main(string[] args)
        {
            new GameManagerService().StarGameProcess();
        }
    }
}
