﻿CREATE TABLE Games(
	[GameId] INT NOT NULL PRIMARY KEY,
	[StartDate]DATETIME NOT NULL,
	[Weight] INT NOT NULL,
	[Attempt] INT NOT NULL,
	[CloseGuess] INT NOT NULL,
	[AlmostWinnerName] VARCHAR (255),
);
